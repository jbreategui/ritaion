export const environment = {
  production: true,
/*   URL_SERVICIOS: 'http://localhost:8000/api',
  URL_SERVER_SOCKETS: 'http://192.168.0.15:5000' */
  URL_SERVICIOS: 'https://ws2-rita.wydnex.com/api',
  URL_SERVER_SOCKETS: 'http://node-rita.wydnex.com:5000'
};
