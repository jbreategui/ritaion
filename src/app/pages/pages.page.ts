import { Component, OnInit } from '@angular/core';
import { SocketsService } from '../sockets/sockets.service';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-pages',
  templateUrl: './pages.page.html',
  styleUrls: ['./pages.page.scss'],
})
export class PagesPage implements OnInit {

  constructor(
    private socket: SocketsService,
    private mat: MatSnackBar
  ) { }

  ngOnInit() {
 
    this.socket.verificarConexion();
   // this.socket.configurarUsuario();
    this.socket.esucharError().subscribe((res: any) => {
      console.log(res);
      this.mat.open('Error', 'Cerrar', { duration: 3000 });
    });
  }

}
