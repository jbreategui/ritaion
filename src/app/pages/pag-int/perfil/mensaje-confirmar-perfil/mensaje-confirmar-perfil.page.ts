import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-mensaje-confirmar-perfil',
  templateUrl: './mensaje-confirmar-perfil.page.html',
  styleUrls: ['./mensaje-confirmar-perfil.page.scss'],
})
export class MensajeConfirmarPerfilPage implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {
  }
  view() {
    this.router.navigate(['/pages/pag-int/home']);
  }
}
