import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PerfilPage } from './perfil.page';


const routes: Routes = [
  {
    path: '', component: PerfilPage, children: [
      { path: '', redirectTo: 'inicio-perfil' },
      { path: 'inicio-perfil', loadChildren: './inicio-perfil/inicio-perfil.module#InicioPerfilPageModule' },
      { path: 'mensaje-error-perfil', loadChildren: './mensaje-error-perfil/mensaje-error-perfil.module#MensajeErrorPerfilPageModule' },
      { path: 'mensaje-confirmar-perfil', loadChildren: './mensaje-confirmar-perfil/mensaje-confirmar-perfil.module#MensajeConfirmarPerfilPageModule' },

    ]
  },


];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class PerfilRoutingModule { }
