import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InicioPerfilPage } from './inicio-perfil.page';

describe('InicioPerfilPage', () => {
  let component: InicioPerfilPage;
  let fixture: ComponentFixture<InicioPerfilPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InicioPerfilPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InicioPerfilPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
