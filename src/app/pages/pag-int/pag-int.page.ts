import { Component, OnInit } from '@angular/core';
import { SocketsService } from '../../sockets/sockets.service';

@Component({
  selector: 'app-pag-int',
  templateUrl: './pag-int.page.html',
  styleUrls: ['./pag-int.page.scss'],
})
export class PagIntPage implements OnInit {

  constructor(private socket: SocketsService) { }

  ngOnInit() {
    this.socket.configurarUsuario();
  }

}
