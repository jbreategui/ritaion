import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RecogerCancelacionPage } from './recoger-cancelacion.page';

describe('RecogerCancelacionPage', () => {
  let component: RecogerCancelacionPage;
  let fixture: ComponentFixture<RecogerCancelacionPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RecogerCancelacionPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RecogerCancelacionPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
