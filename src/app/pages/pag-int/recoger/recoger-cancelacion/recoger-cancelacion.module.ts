import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { RecogerCancelacionPage } from './recoger-cancelacion.page';

const routes: Routes = [
  {
    path: '',
    component: RecogerCancelacionPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [RecogerCancelacionPage]
})
export class RecogerCancelacionPageModule {}
