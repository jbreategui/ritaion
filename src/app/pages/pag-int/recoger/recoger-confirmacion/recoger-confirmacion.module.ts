import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { RecogerConfirmacionPage } from './recoger-confirmacion.page';

const routes: Routes = [
  {
    path: '',
    component: RecogerConfirmacionPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [RecogerConfirmacionPage]
})
export class RecogerConfirmacionPageModule {}
