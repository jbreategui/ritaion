import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-registrar-pago-pos',
  templateUrl: './registrar-pago-pos.page.html',
  styleUrls: ['./registrar-pago-pos.page.scss'],
})
export class RegistrarPagoPosPage implements OnInit {

  constructor(private router: Router) { }
  flag: boolean = true;
  button_flag = false;
  texto: string = 'Preparando, espere por favor';
  ngOnInit() {

    this.preparar().then((res: boolean) => {
      this.flag = res;
      this.button_flag = true;
      this.texto = 'Terminado, recoja su pedido!';


    });
  }


  verificarPago(): Promise<boolean> {

    return new Promise((resolve, reject) => {

      setTimeout(() => {
        resolve(true);
      }, 3000);

    });

  }


  preparar(): Promise<boolean> {

    return new Promise((resolve, reject) => {

      setTimeout(() => {
        resolve(false);
      }, 3000);

    });

  }

  view() {
    this.router.navigate(['/pages/pag-int/recoger/mensaje-confirmar-recoger']);
  }
}
