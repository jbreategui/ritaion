import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { VerificaTokenGuard } from '../../../services/guards/verifica-token.guard';
import { RecogerGuard } from '../../../services/guards/recoger.guard';
import { RecogerPage } from './recoger.page';


const routes: Routes = [
  {
    path: '', component: RecogerPage, children: [
      { path: '', redirectTo: 'lista-pedidos-pagados' },
      {
        canActivate: [VerificaTokenGuard],
        path: 'lista-pedidos-pagados',
        loadChildren: './lista-pedidos-pagados/lista-pedidos-pagados.module#ListaPedidosPagadosPageModule'
      },
      {
        canActivate: [VerificaTokenGuard, RecogerGuard],
        path: 'resumen-pedido-recoger/:id',
        loadChildren: './resumen-pedido-recoger/resumen-pedido-recoger.module#ResumenPedidoRecogerPageModule'
      },
      {
        canActivate: [VerificaTokenGuard, RecogerGuard],
        path: 'qr-recoger/:id',
        loadChildren: './qr-recoger/qr-recoger.module#QrRecogerPageModule'
      },
      {
        canActivate: [VerificaTokenGuard, RecogerGuard],
        path: 'recoger-confirmacion',
        loadChildren: './recoger-confirmacion/recoger-confirmacion.module#RecogerConfirmacionPageModule'
      },
      {
        canActivate: [VerificaTokenGuard, RecogerGuard],
        path: 'recoger-cancelacion',
        loadChildren: './recoger-cancelacion/recoger-cancelacion.module#RecogerCancelacionPageModule'
      }
    ]
  },



];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class RecogerRoutingModule { }
