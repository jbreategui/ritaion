import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ResumenPedidoRecogerPage } from './resumen-pedido-recoger.page';

describe('ResumenPedidoRecogerPage', () => {
  let component: ResumenPedidoRecogerPage;
  let fixture: ComponentFixture<ResumenPedidoRecogerPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ResumenPedidoRecogerPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ResumenPedidoRecogerPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
