import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { ResumenPedidoRecogerPage } from './resumen-pedido-recoger.page';
import { SharedModule } from '../../../../shared/shared.module';
import { MatSnackBarModule } from '@angular/material/snack-bar';

const routes: Routes = [
  {
    path: '',
    component: ResumenPedidoRecogerPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    SharedModule,
    MatSnackBarModule
  ],
  declarations: [ResumenPedidoRecogerPage]
})
export class ResumenPedidoRecogerPageModule {}
