import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { ListaPedidosPagadosPage } from './lista-pedidos-pagados.page';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { SharedModule } from '../../../../shared/shared.module';
 

const routes: Routes = [
  {
    path: '',
    component: ListaPedidosPagadosPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    MatSnackBarModule,
    SharedModule
  
  ],
  declarations: [ListaPedidosPagadosPage]
})
export class ListaPedidosPagadosPageModule {}
