import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { RecogerService } from '../../../../services/recoger/recoger.service';
import { DURATIONSNACK } from '../../../../config/variables';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MetodosNativosService } from '../../../../services/nativo/metodos-nativos.service';


@Component({
  selector: 'app-lista-pedidos-pagados',
  templateUrl: './lista-pedidos-pagados.page.html',
  styleUrls: ['./lista-pedidos-pagados.page.scss'],
})
export class ListaPedidosPagadosPage implements OnInit {
  public arrayRecoger = [];
  public pedido = null;
  public qrCodigo = null;
  constructor(
    private wsRecoger: RecogerService,
    private snackBar: MatSnackBar,
    private router: Router,
    private nat: MetodosNativosService) { }

  ngOnInit() {

  }
  ionViewWillEnter() {

    this.getListaPedidoPagados();

  }

  getListaPedidoPagados() {
    this.nat.obtenerLocalStorage('usuario').then((usuario: any) => {
      console.log(usuario);
      this.wsRecoger.getListaPedidosPagados({ pedido_usuario_id: usuario.usuario_id }).subscribe((res: any) => {
        if (res.estado) {
          this.arrayRecoger = res.payload;
        } else {
          this.snackBar.open('No se encontraron pedidos pagados', 'Cerrar', {
            duration: DURATIONSNACK
          });
        }
      });
    });

  }


  seleccionaProducto() {
    if (this.validarPedido()) {
      this.router.navigate(['/pages/pag-int/recoger/resumen-pedido-recoger', this.pedido]);

    } else {
      this.snackBar.open('Seleccione pedido', 'Cerrar', {
        duration: 3000
      });
    }
  }
  validarPedido() {
    return this.pedido !== null;
  }
  seleccionarPedido(item: number, qr: number) {
    this.pedido = item;
    this.wsRecoger.qrRecoger = qr;
  }

  regresar() {
    this.router.navigate(['/pages/pag-int/home']);
  }
}
