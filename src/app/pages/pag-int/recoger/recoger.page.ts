import { Component, OnInit, OnDestroy } from '@angular/core';
import { RecogerService } from '../../../services/recoger/recoger.service';

@Component({
  selector: 'app-recoger',
  templateUrl: './recoger.page.html',
  styleUrls: ['./recoger.page.scss'],
})
export class RecogerPage implements OnInit, OnDestroy {

  constructor(private session: RecogerService) { }
  ngOnInit() {
    this.session.sessionRecoger = true;
    console.log('inicia recoger ');
  }
  ionViewWillEnter() {
    this.session.sessionRecoger = true;
    console.log('inicio will recoger');

  }

  ionViewWillLeave() {
    this.session.sessionRecoger = false;
    console.log('muere recoger leave');

  }

  ngOnDestroy() {
    this.session.sessionRecoger = false;
    console.log('muere recoger');

  }
}
