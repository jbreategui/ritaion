import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ContactanosPage } from './contactanos.page';

const routes: Routes = [
  {
    path: '', component: ContactanosPage, children: [
      { path: '', redirectTo: 'inicio-contactanos' },
      { path: 'inicio-contactanos', loadChildren: './inicio-contactanos/inicio-contactanos.module#InicioContactanosPageModule' },
      { path: 'recomendar', loadChildren: './recomendar/recomendar.module#RecomendarPageModule' },
      { path: 'buzon', loadChildren: './buzon/buzon.module#BuzonPageModule' },
      { path: 'mensaje-contactanos', loadChildren: './mensaje-contactanos/mensaje-contactanos.module#MensajeContactanosPageModule' },

    ]
  }


];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class ContactanosRoutingModule { }
