import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { ContactanosPage } from './contactanos.page';
import { ContactanosRoutingModule } from './contactanos-routing.module';
 

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ContactanosRoutingModule
  ],
  declarations: [ContactanosPage]
})
export class ContactanosPageModule {}
