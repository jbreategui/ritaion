import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-mensaje-contactanos',
  templateUrl: './mensaje-contactanos.page.html',
  styleUrls: ['./mensaje-contactanos.page.scss'],
})
export class MensajeContactanosPage implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {
  }
 
  view() {
    this.router.navigate(['/pages/pag-int/home']);
  }
}
