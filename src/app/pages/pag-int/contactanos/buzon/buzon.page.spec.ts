import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BuzonPage } from './buzon.page';

describe('BuzonPage', () => {
  let component: BuzonPage;
  let fixture: ComponentFixture<BuzonPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BuzonPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BuzonPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
