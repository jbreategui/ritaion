import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InicioContactanosPage } from './inicio-contactanos.page';

describe('InicioContactanosPage', () => {
  let component: InicioContactanosPage;
  let fixture: ComponentFixture<InicioContactanosPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InicioContactanosPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InicioContactanosPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
