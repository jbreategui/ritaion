import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { PedidoSesionService } from '../../../../sesion/pedido.sesion.service';
import { PedidoService } from '../../../../services/pedido/pedido.service';

@Component({
  selector: 'app-registrar-datos-bancarios',
  templateUrl: './registrar-datos-bancarios.page.html',
  styleUrls: ['./registrar-datos-bancarios.page.scss'],
})
export class RegistrarDatosBancariosPage implements OnInit {

  constructor(
    private router: Router,
    private wsPedido: PedidoService,
    private session: PedidoSesionService


  ) { }

  ngOnInit() {
  }
  getEvento(evento: any) {
    this.wsPedido.iniciarPedido(evento).then((res: any) => {
      console.log('registrando pedido', res);
      this.session.pedidoId = res.payload.pedido_id * 1;
      this.session.ritaId = res.payload.rita_id;
      this.router.navigate(['/pages/pag-int/pedidos/pasarela']);

    }).catch((error) => {
      console.log(error);
    });

  }


}
