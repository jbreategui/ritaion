import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PedidosPage } from './pedidos.page';
import { VerificaTokenGuard } from '../../../services/guards/verifica-token.guard';
import { PedidoGuardGuard } from '../../../services/guards/pedido-guard.guard';


const routes: Routes = [
  {
    path: '', component: PedidosPage, canActivate: [VerificaTokenGuard], children: [
      { path: '', redirectTo: 'ubicacion-rita' },
      {
        canActivate: [VerificaTokenGuard],
        path: 'ubicacion-rita', loadChildren: './ubicacion-rita/ubicacion-rita.module#UbicacionRitaPageModule'
      },
      {
        canActivate: [VerificaTokenGuard, PedidoGuardGuard],
        path: 'mensaje-confirmar-pedido',
        loadChildren: './mensaje-confirmar-pedido/mensaje-confirmar-pedido.module#MensajeConfirmarPedidoPageModule'

      },
      {
        canActivate: [VerificaTokenGuard, PedidoGuardGuard],
        path: 'mensaje-rechazar-pedido',
        loadChildren: './mensaje-rechazar-pedido/mensaje-rechazar-pedido.module#MensajeRechazarPedidoPageModule'
      },
      {
        canActivate: [VerificaTokenGuard, PedidoGuardGuard],
        path: 'pasarela', loadChildren: './pasarela/pasarela.module#PasarelaPageModule'
      },
      {
        canActivate: [VerificaTokenGuard, PedidoGuardGuard],
        path: 'recoger-pedidos', loadChildren: './recoger-pedidos/recoger-pedidos.module#RecogerPedidosPageModule'
      },
      {
        canActivate: [VerificaTokenGuard, PedidoGuardGuard],
        path: 'registrar-datos-bancarios',
        loadChildren: './registrar-datos-bancarios/registrar-datos-bancarios.module#RegistrarDatosBancariosPageModule'
      },
      {
        canActivate: [VerificaTokenGuard, PedidoGuardGuard],
        path: 'resumen-pedido-confirmar',
        loadChildren: './resumen-pedido-confirmar/resumen-pedido-confirmar.module#ResumenPedidoConfirmarPageModule'
      },
      {
        canActivate: [VerificaTokenGuard, PedidoGuardGuard],
        path: 'seleccionar-fruta', loadChildren: './seleccionar-fruta/seleccionar-fruta.module#SeleccionarFrutaPageModule'
      },
      {
        canActivate: [VerificaTokenGuard, PedidoGuardGuard],
        path: 'seleccionar-topping', loadChildren: './seleccionar-topping/seleccionar-topping.module#SeleccionarToppingPageModule'
      },
      { canActivate: [VerificaTokenGuard, PedidoGuardGuard], path: 'qr', loadChildren: './qr/qr.module#QrPageModule' },

    ]
  },




];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class PedidoRoutingModule { }
