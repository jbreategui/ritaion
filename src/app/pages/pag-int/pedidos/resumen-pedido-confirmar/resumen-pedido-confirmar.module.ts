import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { ResumenPedidoConfirmarPage } from './resumen-pedido-confirmar.page';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { SharedModule } from 'src/app/shared/shared.module';

const routes: Routes = [
  {
    path: '',
    component: ResumenPedidoConfirmarPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    MatSnackBarModule,
    SharedModule
  ],
  declarations: [ResumenPedidoConfirmarPage]
})
export class ResumenPedidoConfirmarPageModule {}
