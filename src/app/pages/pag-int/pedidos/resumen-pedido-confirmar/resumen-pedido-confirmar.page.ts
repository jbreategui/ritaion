import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material/snack-bar';
import { PedidoSesionService } from '../../../../sesion/pedido.sesion.service';
import { Fruta, Topping } from '../../../../interface/pedido.interface';
import { IonCheckbox } from '@ionic/angular';
import { Subscription } from 'rxjs';
import { VariablesServices } from '../../../../config/variables';
import { PedidoService } from '../../../../services/pedido/pedido.service';

@Component({
  selector: 'app-resumen-pedido-confirmar',
  templateUrl: './resumen-pedido-confirmar.page.html',
  styleUrls: ['./resumen-pedido-confirmar.page.scss'],
})
export class ResumenPedidoConfirmarPage implements OnInit {
  /*   @ViewChild('check1', { static: false }) check1: IonCheckbox;
    @ViewChild('check2', { static: false }) check2: IonCheckbox; */
  flag = false;
  /*   checked1 = null;
    checked2 = null;
    formaPago = 0;
    subsCheck1: Subscription = new Subscription();
    subsCheck2: Subscription = new Subscription();
   */
  public frutas: Fruta[] = [];
  public topping: Topping;
  public montoTotal = 0.0;
  constructor(
    private router: Router,
    private snackBar: MatSnackBar,
    private session: PedidoSesionService,
    private env: VariablesServices,
    private wsPedido: PedidoService
  ) {

  }

  ngOnInit() {

  }
  ionViewWillLeave() {
 /*    this.subsCheck1.unsubscribe();
    this.subsCheck2.unsubscribe() */;
    console.log('desrtoy');

  }

  ionViewWillEnter() {
    this.getResumen();
    /*     this.subsCheck1 = this.check1.ionChange.subscribe((res: CustomEvent) => {
          const bol = res.detail.checked;
          if (bol) {
            this.check2.checked = false;
            this.formaPago = 1; // forma de pago presencial (POS)
          }
        });
        this.subsCheck2 = this.check2.ionChange.subscribe((res: CustomEvent) => {
          const bol = res.detail.checked;
          if (bol) {
            this.check1.checked = false;
            this.formaPago = 2; // forma de pago via app
          }
        }); */
  }

  destructurar() {

    const frutas = this.session.datosFruta;
    const desFrutas = [];
    for (const iterator of frutas) {
      desFrutas.push({ ...iterator });
    }
    return desFrutas;
  }

  getResumen() {
    this.montoTotal = 0.0;
    this.montoTotal = 0.0;
    let bol = true;
    this.frutas = this.destructurar();
    let precio = 0.0;
    for (const item of this.frutas) {
      if (bol) {
        precio = item.fruta_precio * 1 + this.session.precioBase * 1;
        item.fruta_precio = precio;
      }
      this.montoTotal += item.fruta_precio * 1;
      bol = false;
    }

    if (this.session.datosTopping !== null) {
      this.flag = true;
      this.topping = this.session.datosTopping;
      this.montoTotal += this.topping.topping_precio * 1;
    } else {
      this.flag = false;
    }

    this.session.montoTotal = this.montoTotal;
  }

  editarFruta() {
    this.limpiarSessionProductos();
    this.router.navigate(['/pages/pag-int/pedidos/seleccionar-fruta']);
  }

  editarTopping() {
    this.session.datosTopping = null;
    this.router.navigate(['/pages/pag-int/pedidos/seleccionar-topping']);
  }
  vaciar() {

    this.session.datosMapaRita = null;
    this.limpiarSessionProductos();
    this.router.navigate(['/pages/pag-int/pedidos/ubicacion-rita']);

  }

  limpiarSessionProductos() {
    this.session.datosTopping = null;
    this.session.datosFruta = [];
  }
  confirmar() {
    this.router.navigate(['/pages/pag-int/pedidos/registrar-datos-bancarios']);

  }




}
