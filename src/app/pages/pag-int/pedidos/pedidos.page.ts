import { Component, OnInit, OnDestroy } from '@angular/core';
import { PedidoService } from '../../../services/pedido/pedido.service';

@Component({
  selector: 'app-pedidos',
  templateUrl: './pedidos.page.html',
  styleUrls: ['./pedidos.page.scss'],
})
export class PedidosPage implements OnInit, OnDestroy {

  constructor(private pedido: PedidoService) { }
  ngOnInit() {
    this.pedido.sessionPedido = true;
  }
  ionViewWillEnter() {
    this.pedido.sessionPedido = true;
  }
  
  ionViewWillLeave() {
    this.pedido.sessionPedido = false;
  }

  ngOnDestroy() {
    this.pedido.sessionPedido = false;
  }

}
