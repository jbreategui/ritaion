import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { SeleccionarToppingPage } from './seleccionar-topping.page';
import { SharedModule } from '../../../../shared/shared.module';
import { MatSnackBarModule } from '@angular/material/snack-bar';

const routes: Routes = [
  {
    path: '',
    component: SeleccionarToppingPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    SharedModule,
    MatSnackBarModule
  ],
  declarations: [SeleccionarToppingPage]
})
export class SeleccionarToppingPageModule {}
