import { Component, OnInit, OnDestroy } from '@angular/core';
import { PedidoService } from '../../../../services/pedido/pedido.service';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material/snack-bar';
import * as $ from 'jquery';
import { DURATIONSNACK } from 'src/app/config/variables';
import { Topping, Fruta } from '../../../../interface/pedido.interface';
import { PedidoSesionService } from '../../../../sesion/pedido.sesion.service';

@Component({
  selector: 'app-seleccionar-topping',
  templateUrl: './seleccionar-topping.page.html',
  styleUrls: ['./seleccionar-topping.page.css'],
})
export class SeleccionarToppingPage implements OnInit, OnDestroy {

  public listaTopping: Topping[];
  public strPrecio = 0.0;
  public toppingId = null;
  public id = 0;
  public topping: Topping;
  constructor(
    private wsPedido: PedidoService,
    private router: Router,
    private snackBar: MatSnackBar,
    private session: PedidoSesionService
  ) { }

  ionViewWillEnter() {
    this.getListatoppings();
  }
  ngOnInit() {

  }
  getListatoppings() {
    this.wsPedido.getListaToppings().subscribe((res) => {
      this.listaTopping = res.payload;

    });
  }

  seleccionarTopping(topping: Topping) {

    for (const item of this.listaTopping) {
      if (item.topping_id === topping.topping_id) {
        this.toppingId = item.topping_id;
        this.topping = topping;
        this.strPrecio = item.topping_precio;
        break;
      }
    }

  }
  regFrutas() {

    if (this.toppingId !== null) {
      this.session.datosTopping = this.topping;
      this.router.navigate(['/pages/pag-int/pedidos/resumen-pedido-confirmar']);
    } else {
      this.snackBar.open('Seleccione topping', 'Cerrar', { duration: DURATIONSNACK });
    }
  }

  omitir() {
    this.session.datosTopping = null;
    this.router.navigate(['/pages/pag-int/pedidos/resumen-pedido-confirmar']);
  }


  stilos(item: Topping) {
    this.snackBar.dismiss();
    this.seleccionarTopping(item);
    if (this.id === 0) {
      $('#' + item.topping_id).addClass('highlight');
      this.id = item.topping_id;
    } else {

      $('#' + this.id).removeClass('highlight');
      $('#' + item.topping_id).addClass('highlight');
      this.id = item.topping_id;
    }
  }
  ngOnDestroy() {
    this.snackBar.dismiss();
  }

  ionViewWillLeave() {
    this.snackBar.dismiss();

  }
}
