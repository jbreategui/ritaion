import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { MensajeRechazarPedidoPage } from './mensaje-rechazar-pedido.page';

const routes: Routes = [
  {
    path: '',
    component: MensajeRechazarPedidoPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [MensajeRechazarPedidoPage]
})
export class MensajeRechazarPedidoPageModule {}
