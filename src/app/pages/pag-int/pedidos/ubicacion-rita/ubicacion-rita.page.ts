import { Component, OnInit, OnDestroy } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MapaService } from 'src/app/services/mapas/mapa.service';
import { DURATIONSNACK } from '../../../../config/variables';
import { Router } from '@angular/router';
import { MapaRita } from '../../../../interface/pedido.interface';
import { PedidoSesionService } from '../../../../sesion/pedido.sesion.service';

@Component({
  selector: 'app-ubicacion-rita',
  templateUrl: './ubicacion-rita.page.html',
  styleUrls: ['./ubicacion-rita.page.scss'],
})
export class UbicacionRitaPage implements OnInit, OnDestroy {

  public list = [];
  previous: any;
  lat = -12.070831; // desde el movil
  lng = -77.031981; // desde el movil

  constructor(
    private serviceRita: MapaService,
    private snackBar: MatSnackBar,
    private router: Router,
    private session: PedidoSesionService
  ) {

  }
  ngOnInit() {

    this.getRitas();

  }
  ngOnDestroy() {
    this.snackBar.dismiss();
  }
  getRitas() {

    this.serviceRita.listaRitas().subscribe((res) => {
      if (res.estado) {
        if (res.payload.length > 0) {
          this.list = res.payload;
        } else {
          this.snackBar.open('No se encontraron ritas', 'Cerrar', { duration: DURATIONSNACK });
        }
      } else {
        this.snackBar.open(res.mensaje, 'Cerrar', { duration: DURATIONSNACK });
      }
    }, error => {
      this.snackBar.open(error, 'Cerrar', { duration: DURATIONSNACK });
    });
  }

  obtenerRita(obj: MapaRita) {
    console.log(obj);
    this.session.datosMapaRita = obj;
    this.router.navigate(['/pages/pag-int/pedidos/seleccionar-fruta']);
    this.session.ritaCodigoSocket = this.session.datosMapaRita.rita_id + this.session.datosMapaRita.rita_codigo;

  }
  clickedMarker(infowindow: any) {
    if (this.previous) {
      this.previous.close();
    }
    this.previous = infowindow;
  }

}
