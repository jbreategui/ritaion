import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { PedidoService } from 'src/app/services/pedido/pedido.service';
import { PedidoSesionService } from '../../../../sesion/pedido.sesion.service';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-pasarela',
  templateUrl: './pasarela.page.html',
  styleUrls: ['./pasarela.page.scss'],
})
export class PasarelaPage implements OnInit {


  constructor(
    private router: Router,
    private wSpedido: PedidoService,
    public snackBar: MatSnackBar,
    private session: PedidoSesionService
  ) { }

  ngOnInit() {
  }
  enviar() {
    const id = this.session.pedidoId;
    const pos = [];
    const pedido = { id, pos };
    this.wSpedido.registrarPagoApp(pedido).subscribe((res: any) => {

      if (res.estado) {
        this.session.qrPedido = res.payload;
        this.router.navigate(['/pages/pag-int/pedidos/qr']);

      } else {
        this.snackBar.open('Hubo un error en el pago', 'Cerrar', {
          duration: 3000
        });
      }

    }, () => {
      this.snackBar.open('Hubo un error en el pago', 'Cerrar', {
        duration: 3000
      });
    });


  }
}
