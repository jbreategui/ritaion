import { Component, OnInit } from '@angular/core';
import { PedidoService } from '../../../../services/pedido/pedido.service';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material/snack-bar';
import * as $ from 'jquery';
import { VariablesServices } from '../../../../config/variables';
import { MetodosService } from '../../../../services/metodos/metodos.service';
import { Observable } from 'rxjs';
import { Fruta } from '../../../../interface/pedido.interface';
import { PedidoSesionService } from '../../../../sesion/pedido.sesion.service';
@Component({
  selector: 'app-seleccionar-fruta',
  templateUrl: './seleccionar-fruta.page.html',
  styleUrls: ['./seleccionar-fruta.page.scss'],
})
export class SeleccionarFrutaPage implements OnInit {
  public listFrutas: Fruta[];
  public listTopping: any[];
  public precio: number = null;
  public strPrecio = 0.0;
  public frutaId: number = null;
  public id = 0;
  public maxFrutas = 3;
  flag = true;

  array: any[] = [];

  constructor(
    private wsPedido: PedidoService,
    private router: Router,
    private snackBar: MatSnackBar,
    private ms: MetodosService,
    private env: VariablesServices,
    private session: PedidoSesionService
  ) { }

  ngOnInit() {
  }

  ionViewWillEnter() {
    this.strPrecio = 0.0;
    this.precio = 0.0;
    this.listFrutas = [];
    this.listTopping = [];
    this.precio = 0.0;
    this.frutaId = 0.0;
    this.id = 0;
    this.flag = true;
    this.array = [];
    this.getFrutas();
  }
  getFrutas() {
    this.wsPedido.getListaFrutas().subscribe(
      (res) => {
        if (res.payload.length > 0) {
          this.listFrutas = res.payload;
        } else {
          this.leerObservable().subscribe((res1) => {
            if (res1 !== false) {
              this.listFrutas = res1.payload;
            }
          });
        }
      },
      () => {
        this.leerObservable().subscribe((res: any) => {
          if (res !== false) {
            this.listFrutas = res.payload;
          }
        });
      }
    );
  }

  regFrutas() {
    if (this.validarCantidad()) {
      if (this.validaItems()) {
        this.session.datosFruta = this.getDatosFrutas();
        this.router.navigate(['/pages/pag-int/pedidos/seleccionar-topping']);
      } else {
        this.snackBar.open('Seleccione fruta', 'Cerrar', {
          duration: this.env.DURATIONSNACK
        });
      }
    } else {
      this.snackBar.open('Seleccione tres frutas como máximo', 'Cerrar', {
        duration: this.env.DURATIONSNACK
      });
    }
  }

  seleccionarFrsuta(param: number) {
    for (const item of this.listFrutas) {
      if (item.fruta_id === param) {
        this.frutaId = item.fruta_id;
        this.precio = item.fruta_precio;
        this.strPrecio = item.fruta_precio;
        break;
      }
    }
  }

  seleccionarFruta(item: Fruta) {
    if (!this.frutaExiste(item.fruta_id)) {
      this.array.push({
        fruta: item.fruta_id,
        estado: 1,
        precio: item.fruta_precio,
        objFruta: item
      });

    }
    this.calcularPrecioTotal(this.array);
  }

  validaItems() {
    if (this.array.length > 0) {
      for (const item of this.array) {
        if (item.estado === 1) {
          return true;
        }
      }
    }
    return false;
  }

  frutaExiste(id: number) {
    let index = 0;
    for (const item of this.array) {
      if (item.fruta === id) {
        if (this.array[index].estado === 0) {
          this.array[index].estado = 1;
        } else {
          this.array[index].estado = 0;
        }
        return true;
      }
      index++;
    }
    return false;
  }

  calcularPrecioTotal(frutas: any[]) {
    let sum = 0;
    let bol = false;
    for (const item of frutas) {
      if (item.estado !== 0) {
        sum += item.precio * 1;
        bol = true;
      }
    }
    if (bol) {
      if (this.flag) {
        this.strPrecio = sum + this.session.precioBase * 1;
      } else {
        this.strPrecio += sum * 1;
      }
    } else {
      this.strPrecio = 0;
    }
    //  console.log(sum);
  }


  stilos(item: Fruta) {
    this.snackBar.dismiss();
    this.id = item.fruta_id;
    if ($('#' + this.id + '_99').hasClass('highlight')) {
      $('#' + this.id + '_99').removeClass('highlight');
      this.seleccionarFruta(item);
    } else {
      if (this.validar()) {
        $('#' + item.fruta_id + '_99').addClass('highlight');
        this.seleccionarFruta(item);
      }
    }
  }

  getDatosFrutas() {
    const arreglo: any[] = [];
    if (this.array.length > 0) {
      for (const item of this.array) {
        if (item.estado === 1) {
          arreglo.push(item.objFruta);
        }
      }
    }
    return arreglo;
  }

  validarCantidad() {
    let a = 0;
    if (this.array.length > 0) {
      for (const item of this.array) {
        if (item.estado === 1) {
          a++;
        }
      }
      if (a > this.maxFrutas) {  // cambia el maximo DE ITEMS) {  // cambia el maximo DE ITEMS
        return false;
      }
    }
    return true;
  }

  validar() {
    let a = 0;
    if (this.array.length > 0) {
      for (const item of this.array) {
        if (item.estado === 1) {
          a++;
          console.log('frutas seleccionadas:', a);
        }
      }
      if (a === this.maxFrutas) { // cambia el maximo DE ITEMS
        this.snackBar.open(`Solo puede elegir ${this.maxFrutas} frutas como máximo`, 'Cerrar', {
          duration: this.env.DURATIONSNACK
        });
        return false;
      }
    }
    return true;
  }

  leerObservable(): Observable<any> {
    return new Observable(observer => {
      let contador = 1;
      const intervalo = setInterval(() => {
        if (this.env.MOD_NUMERO_MAX_PETICIONES >= contador) {
          this.snackBar.dismiss();
          this.ms.contador = 0;
          this.wsPedido.getListaFrutas().subscribe(
            (res: any) => {
              console.log(contador, res);
              if (res.estado) {
                if (res.payload.length > 0) {
                  observer.next(res);
                  clearInterval(intervalo);
                  observer.complete();
                } else {
                  observer.next(false);
                }
              } else {
                observer.next(false);
              }
            },
            err => {
              this.snackBar.open(err.name, 'Cerrar');
            }
          );
        } else {
          clearInterval(intervalo);
          observer.complete();
        }
        contador += 1;
      }, this.env.MOD_INTERVALO_PETICIONES * 1000);
    });
  }


}
