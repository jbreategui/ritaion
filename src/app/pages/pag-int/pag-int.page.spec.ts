import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PagIntPage } from './pag-int.page';

describe('PagIntPage', () => {
  let component: PagIntPage;
  let fixture: ComponentFixture<PagIntPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PagIntPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PagIntPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
