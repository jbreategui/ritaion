import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { RecogerPedidoPage } from './recoger-pedido.page';

const routes: Routes = [
  {
    path: '',
    component: RecogerPedidoPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [RecogerPedidoPage]
})
export class RecogerPedidoPageModule {}
