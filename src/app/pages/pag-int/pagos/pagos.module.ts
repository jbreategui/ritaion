import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { PagosPage } from './pagos.page';
import { PagosRoutingModule } from './pagos-routing.module';

 

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PagosRoutingModule,
  ],
  declarations: [PagosPage]
})
export class PagosPageModule {}
