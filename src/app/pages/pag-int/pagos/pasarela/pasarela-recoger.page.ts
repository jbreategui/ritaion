import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { PedidoService } from '../../../../services/pedido/pedido.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { PagarService } from '../../../../services/pago/pagar.service';

@Component({
  selector: 'app-pasarela-recoger',
  templateUrl: './pasarela-recoger.page.html',
  styleUrls: ['./pasarela-recoger.page.scss'],
})
export class PasarelaRecogerPage implements OnInit {

  constructor(
    private router: Router,
    private wSpedido: PedidoService,
    public snackBar: MatSnackBar,
    public ws: PagarService
  ) { }

  ngOnInit() {
  }





  obtenerDatos(pos: any) {
    console.log(pos);
    const id = this.ws.pedidioIDRecoger;
    const pedido = { id, pos };
    this.wSpedido.registrarPagoApp(pedido).subscribe((res: any) => {
      console.log(res);
      if (res.estado) {
        this.ws.pedidioIDqr = res.payload;
        this.router.navigate(['/pages/pag-int/pagos/leer-qr-recoger']);


      } else {
        this.snackBar.open('Hubo un error en el pago', 'Cerrar', {
          duration: 3000
        });
      }

    }, () => {
      this.snackBar.open('Hubo un error en el pago', 'Cerrar', {
        duration: 3000
      });
    });


  }
}
