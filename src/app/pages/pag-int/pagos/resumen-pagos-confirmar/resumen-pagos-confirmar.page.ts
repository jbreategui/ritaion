import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { PagarService } from '../../../../services/pago/pagar.service';
import { PedidoSesionService } from '../../../../sesion/pedido.sesion.service';

@Component({
  selector: 'app-resumen-pagos-confirmar',
  templateUrl: './resumen-pagos-confirmar.page.html',
  styleUrls: ['./resumen-pagos-confirmar.page.scss'],
})
export class ResumenPagosConfirmarPage implements OnInit {
  montoTotal = 0.0;
  frutas: any = [];
  topping: any = null;
  pedidoId = 0;

  constructor(
    private route: Router,
    private ws: PagarService,
    private pd: PedidoSesionService
  ) { }

  ngOnInit() {

    this.montoTotal = 0;
    let precio = 0.0;
    let bol = true;
    this.frutas = this.ws.sessionPagar.frutas;
    this.topping = this.ws.sessionPagar.topping[0];
    for (const item of this.frutas) {
      if (bol) {
        precio = item.fruta_precio * 1 + this.pd.precioBase * 1;
        item.fruta_precio = precio;
      }
      this.montoTotal += item.fruta_precio * 1;
      bol = false;
    }
    if (this.topping) {
      this.montoTotal += this.topping.topping_precio * 1;
    }
  }
  cancelar() {
    this.route.navigate(['/pages/pag-int/home']);
  }
  confirmar() {
    this.route.navigate(['/pages/pag-int/pagos/datos-bancarios']);
  }
}
