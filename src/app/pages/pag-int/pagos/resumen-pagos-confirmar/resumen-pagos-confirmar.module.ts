import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { ResumenPagosConfirmarPage } from './resumen-pagos-confirmar.page';
import { ComponentsModule } from '../../../../components/components.module';
import { SharedModule } from '../../../../shared/shared.module';

const routes: Routes = [
  {
    path: '',
    component: ResumenPagosConfirmarPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    ComponentsModule,
    SharedModule
  ],
  declarations: [ResumenPagosConfirmarPage]
})
export class ResumenPagosConfirmarPageModule { }
