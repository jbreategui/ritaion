import { Component, OnInit, OnDestroy } from '@angular/core';
import { PagarService } from '../../../services/pago/pagar.service';

@Component({
  selector: 'app-pagos',
  templateUrl: './pagos.page.html',
  styleUrls: ['./pagos.page.scss'],
})
export class PagosPage implements OnInit, OnDestroy {
  constructor(private session: PagarService) { }
  ngOnInit() {
    this.session.sessionPagar = true;
    console.log('inicia pagar ');
  }
  ionViewWillEnter() {
    this.session.sessionPagar = true;
    console.log('inicio will pagar');

  }

  ionViewWillLeave() {
    this.session.sessionPagar = false;
    console.log('muere pagar leave');

  }

  ngOnDestroy() {
    this.session.sessionPagar = false;
    console.log('muere pagar');

  }

}
