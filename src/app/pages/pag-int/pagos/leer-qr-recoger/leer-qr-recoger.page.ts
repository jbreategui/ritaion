import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { PedidoSesionService } from '../../../../sesion/pedido.sesion.service';
import { PedidoSocketService } from '../../../../services/pedido/pedido-socket.service';
import { MetodosNativosService } from '../../../../services/nativo/metodos-nativos.service';
import { Router } from '@angular/router';
import { PagarService } from '../../../../services/pago/pagar.service';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { CryptoService } from '../../../../services/crypto/crypto.service';

@Component({
  selector: 'app-leer-qr-recoger',
  templateUrl: './leer-qr-recoger.page.html',
  styleUrls: ['./leer-qr-recoger.page.scss'],
})
export class LeerQrRecogerPage implements OnInit, OnDestroy {

  value = '';
  rita$: Subscription = new Subscription();
  cargando = false;
  mensaje = '';
  constructor(
    private session: PedidoSesionService,
    private sPedido: PedidoSocketService,
    private nat: MetodosNativosService,
    private router: Router,
    private pagar: PagarService,
    private crypto: CryptoService
  ) { }

  ngOnInit() {
    this.preparar();
  }
  ionViewWillEnter() {
    this.escucharRitaDisponible();

  }
  preparar() {

    this.nat.obtenerLocalStorage('ID_TABLA').then((usuario: any) => {
      const pedidoId = this.pagar.pedidioIDqr;
      const clienteId = usuario;
      const cry = this.crypto.set(`${pedidoId} % ${clienteId}%`);
      this.value = encodeURIComponent(cry) + 'abc';
    }, () => {
      this.mensaje = '';
    });
  }
  escucharRitaDisponible() {

    this.rita$ = this.sPedido.escucharRitaEstado().
      subscribe((res: any) => {
        console.log(res);
        const tipo = res.payload.tipoMensaje * 1;
        this.cargando = true;
        this.mensaje = 'Su pedido se está procesando';
        switch (tipo) {
          case 1:
            this.router.navigate(['/pages/pag-int/pagos/mensaje-rechazar-pagos']);
            break;
          case 2:
            this.cargando = true;
            this.mensaje = res.payload.mensaje;
            break;
          case 3:
            this.router.navigate(['/pages/pag-int/pagos/mensaje-confirmar-pagos']);
            break;
          default:
            this.router.navigate(['/pages/pag-int/pagos/mensaje-rechazar-pagos']);
            break;
        }

      }, () => {
        this.cargando = false;
        this.router.navigate(['/pages/pag-int/pagos/mensaje-rechazar-pagos']);
        this.mensaje = '';
      }, () => {
        this.cargando = false;
        this.mensaje = '';

      });
  }

  ionViewWillLeave() {

    this.rita$.unsubscribe();
  }

  ngOnDestroy() {

    this.rita$.unsubscribe();
  }
  home() {
    this.router.navigate(['/pages/pag-int/home']);
  }
}
