import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material/snack-bar';
import { PagarService } from '../../../../services/pago/pagar.service';
import { MetodosNativosService } from '../../../../services/nativo/metodos-nativos.service';

@Component({
  selector: 'app-leer-qr',
  templateUrl: './leer-codigo.page.html',
  styleUrls: ['./leer-codigo.page.scss'],
})
export class LeerCodigoPage implements OnInit {
  codigo = null;
  constructor(
    private route: Router,
    private snack: MatSnackBar,
    private ws: PagarService,
    private nat: MetodosNativosService

  ) { }

  ngOnInit() {
  }


  validarCodigoPedido() {

    this.nat.obtenerLocalStorage('usuario').then((item: any) => {
      console.log(item);
      if (this.codigo != null) {
        this.ws.getValidarCodigo({ codigo_pedido: this.codigo, usuarioId: item.usuario_id }).subscribe((res: any) => {
          if (res.estado) {
            console.log(this.ws.sessionPagar);
            this.ws.sessionPagar = res.payload;
            this.ws.pedidioIDRecoger = res.pedido_id;
            this.route.navigate(['/pages/pag-int/pagos/resumen-pagos-confirmar']);
          } else {
            this.snack.open(res.mensaje, 'Cerrar', { duration: 3000 });
          }
        });
      } else {
        this.snack.open('Ingrese código', 'Cerrar', { duration: 3000 });
      }


    }, (res1) => {
      console.log(res1);
    });

  }
}
