import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { SharedModule } from '../../../../shared/shared.module';
import { QRScanner } from '@ionic-native/qr-scanner/ngx';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { LeerCodigoPage } from './leer-codigo.page';
const routes: Routes = [
  {
    path: '',
    component: LeerCodigoPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    SharedModule,
    MatSnackBarModule
  ],
  declarations: [LeerCodigoPage],
  providers: [QRScanner]
})
export class LeerCodigoPageModule { }
