import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DatosBancariosPage } from './datos-bancarios.page';

describe('DatosBancariosPage', () => {
  let component: DatosBancariosPage;
  let fixture: ComponentFixture<DatosBancariosPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DatosBancariosPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DatosBancariosPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
