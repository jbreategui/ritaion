import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-datos-bancarios',
  templateUrl: './datos-bancarios.page.html',
  styleUrls: ['./datos-bancarios.page.scss'],
})
export class DatosBancariosPage implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {
  }
  getEvento(evento: string) {

    console.log(evento);
    this.router.navigate(['/pages/pag-int/pagos/pasarela']);
  }
}
