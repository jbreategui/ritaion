import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-mensaje-public',
  templateUrl: './mensaje-public.page.html',
  styleUrls: ['./mensaje-public.page.scss'],
})
export class MensajePublicPage implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {
  }

  view() {
    this.router.navigate(['/login']);
  }
}
