import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MensajePublicPage } from './mensaje-public.page';

describe('MensajePublicPage', () => {
  let component: MensajePublicPage;
  let fixture: ComponentFixture<MensajePublicPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MensajePublicPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MensajePublicPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
