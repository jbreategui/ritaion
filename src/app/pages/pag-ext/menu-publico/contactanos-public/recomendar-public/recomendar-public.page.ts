import { Component, OnInit } from '@angular/core';
import { MetodosNativosService } from '../../../../../services/nativo/metodos-nativos.service';

@Component({
  selector: 'app-recomendar-public',
  templateUrl: './recomendar-public.page.html',
  styleUrls: ['./recomendar-public.page.scss'],
})
export class RecomendarPublicPage implements OnInit {

  constructor(private native: MetodosNativosService) { }

  ngOnInit() {
  }
  compartir(tipo: number) {

    switch (tipo) {
      case 1:
        this.native.compartir().shareViaFacebook('', '', 'https://www.rita.com.pe/');

        break;
      case 2:
        this.native.compartir().shareViaInstagram('https://www.rita.com.pe/', '');

        break;
      case 3:
        this.native.compartir().shareViaTwitter('Hola!', '', 'https://www.rita.com.pe/');
        break;
      default:
        break;
    }
  }
}
