import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RecomendarPublicPage } from './recomendar-public.page';

describe('RecomendarPublicPage', () => {
  let component: RecomendarPublicPage;
  let fixture: ComponentFixture<RecomendarPublicPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RecomendarPublicPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RecomendarPublicPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
