import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BuzonPublicPage } from './buzon-public.page';

describe('BuzonPublicPage', () => {
  let component: BuzonPublicPage;
  let fixture: ComponentFixture<BuzonPublicPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BuzonPublicPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BuzonPublicPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
