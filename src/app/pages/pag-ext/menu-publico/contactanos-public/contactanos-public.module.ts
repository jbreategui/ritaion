import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
 
import { IonicModule } from '@ionic/angular';

import { ContactanosPublicPage } from './contactanos-public.page';
import { ContactanosPublicoRoutingModule } from './contactanos-public-routing.module';



@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ContactanosPublicoRoutingModule
  ],
  declarations: [ContactanosPublicPage]
})
export class ContactanosPublicPageModule { }
