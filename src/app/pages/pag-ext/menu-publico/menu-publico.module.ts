import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { MenuPublicoPage } from './menu-publico.page';
import { MenuPublicoRoutingModule } from './menu-publico-routing.module';



@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MenuPublicoRoutingModule
  ],
  declarations: [MenuPublicoPage]
})
export class MenuPublicoPageModule { }
