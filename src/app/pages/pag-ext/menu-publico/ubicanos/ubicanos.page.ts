import { Component, OnInit } from '@angular/core';
import { MapaService } from '../../../../services/mapas/mapa.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { PedidoSesionService } from '../../../../sesion/pedido.sesion.service';
import { DURATIONSNACK } from '../../../../config/variables';

@Component({
  selector: 'app-ubicanos',
  templateUrl: './ubicanos.page.html',
  styleUrls: ['./ubicanos.page.scss'],
})
export class UbicanosPage implements OnInit {

  public list = [];
  previous: any;
  lat = -12.070831; // desde el movil
  lng = -77.031981; // desde el movil

  constructor(
    private serviceRita: MapaService,
    private snackBar: MatSnackBar,
    private router: Router,
    private session: PedidoSesionService
  ) {

  }
  ngOnInit() {

    this.getRitas();

  }

  getRitas() {

    this.serviceRita.listaRitasPublic().subscribe((res) => {
      if (res.estado) {
        if (res.payload.length > 0) {
          this.list = res.payload;
        } else {
          this.snackBar.open('No se encontraron ritas', 'Cerrar', { duration: DURATIONSNACK });
        }
      } else {
        this.snackBar.open(res.mensaje, 'Cerrar', { duration: DURATIONSNACK });
      }
    }, error => {
      this.snackBar.open(error, 'Cerrar', { duration: DURATIONSNACK });
    });
  }

  clickedMarker(infowindow: any) {
    if (this.previous) {
      this.previous.close();
    }
    this.previous = infowindow;
  }

}
