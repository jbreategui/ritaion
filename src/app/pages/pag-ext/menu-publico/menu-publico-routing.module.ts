import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MenuPublicoPage } from './menu-publico.page';

const routes: Routes = [
  {
    path: '', component: MenuPublicoPage, children: [
      { path: '', redirectTo: 'login' },
      { path: 'ubicanos', loadChildren: './ubicanos/ubicanos.module#UbicanosPageModule' },
      { path: 'contactanos-public', loadChildren: './contactanos-public/contactanos-public.module#ContactanosPublicPageModule' }

    ]
  },

];

@NgModule({
  declarations: [],
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class MenuPublicoRoutingModule { }
