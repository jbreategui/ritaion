import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { UsuarioService } from 'src/app/services/usuario/usuario.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { DURATIONSNACK } from 'src/app/config/variables';

@Component({
  selector: 'app-recuperar-password',
  templateUrl: './recuperar-password.page.html',
  styleUrls: ['./recuperar-password.page.scss'],
})
export class RecuperarPasswordPage implements OnInit {
  formulario: FormGroup
  loading: boolean = false;
  constructor(private usuario: UsuarioService, private snackBar: MatSnackBar, private router: Router) { }
  private pattern = "[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$";
  ngOnInit() {
    this.formulario = new FormGroup({
      'email': new FormControl('', [Validators.required, Validators.pattern(this.pattern)])
    });


  }
  body: object = {
    email: ''
  };


  resetPassword() {

    if (this.validar()) {
      this.loading = true;
      let body = JSON.stringify(this.formulario.value);
      this.usuario.getRecuperarPassword(body)
        .subscribe((data: any) => {
          this.snackBar.open(data.mensaje, 'Cerrar', { duration: DURATIONSNACK })
          this.loading = false;

          if (data.estado) {
            this.router.navigate(['/pages/pag-ext/mensaje-confirmacion-login']);
          }

        }, err => {

          this.loading = false;
          this.snackBar.open(err.mensaje, 'Cerrar', { duration: DURATIONSNACK })
        });

    }

  }

  validar() {

    if (this.formulario.controls['email'].valid) {
      return true;
    }
    else {
      this.snackBar.open('El email no es válido', 'cerrar', { duration: DURATIONSNACK });
      return false;
    }
  }
  view() {
    this.router.navigate(['/login']);
  }


  
}
