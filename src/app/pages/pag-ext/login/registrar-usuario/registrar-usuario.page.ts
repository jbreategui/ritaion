import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { UbigeoService } from '../../../../services/ubigeo/ubigeo.service';
import { UsuarioService } from '../../../../services/usuario/usuario.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MetodosService } from '../../../../services/metodos/metodos.service';
import { Router } from '@angular/router';
import { DURATIONSNACK } from 'src/app/config/variables';

@Component({
  selector: 'app-registrar-usuario',
  templateUrl: './registrar-usuario.page.html',
  styleUrls: ['./registrar-usuario.page.scss'],
})
export class RegistrarUsuarioPage implements OnInit {

  private pattern = "[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$";
  public formulario: FormGroup;



  constructor(private ubigeo: UbigeoService,
    private usuario: UsuarioService,
    private snackBar: MatSnackBar, public mensaje: MetodosService, public router: Router) {

  }

  ngOnInit() {

    this.formulario = new FormGroup({
      'persona_nombre': new FormControl('', Validators.required),
      'persona_apellido': new FormControl('', Validators.required),
      'persona_telefono': new FormControl('', Validators.required),
      'persona_correo': new FormControl('', [Validators.required, Validators.pattern(this.pattern)]),
      'password_usuario': new FormControl('', Validators.required)
    });
  }


  crear() {

    if (this.validarFormulario()) {
      this.usuario.registrarUsuario(JSON.stringify(this.formulario.value)).subscribe((res: any) => {

        if (res.estado) {

          this.router.navigate(['/pages/pag-ext/mensaje-confirmacion-login']);
        }
        else {
          this.snackBar.open(res.mensaje, 'cerrar', { duration: DURATIONSNACK });
        }
      }, er => {
        console.log(er);
        this.snackBar.open(er.name, 'cerrar', { duration: DURATIONSNACK });
      });
    }

  }


  validarFormulario() {

    if (this.formulario.controls['persona_nombre'].invalid) {
      this.snackBar.open('Ingrese nombre', 'cerrar', { duration: DURATIONSNACK });
      return false;
    }
    else if (this.formulario.controls['persona_apellido'].invalid) {
      this.snackBar.open('Ingrese apellido', 'cerrar', { duration: DURATIONSNACK });
      return false;
    }
    else if (this.formulario.controls['persona_telefono'].invalid) {
      this.snackBar.open('Ingrese teléfono', 'cerrar', { duration: DURATIONSNACK });
      return false;
    }
    else if (this.formulario.controls['persona_correo'].invalid) {
      this.snackBar.open('Correo inválido', 'cerrar', { duration: DURATIONSNACK });
      return false;
    }
    else if (this.formulario.controls['password_usuario'].invalid) {
      this.snackBar.open('Ingrese contraseña', 'cerrar', { duration: DURATIONSNACK });
      return false;
    }
    else {

      return true;
    }

  }

  view() {
    this.router.navigate(['/login']);
  }

  
}
