import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginPage } from './login.page';

const routes: Routes = [
  {
    path: '', component: LoginPage, children: [
      { path: '', redirectTo: 'registrar' },
      { path: 'recuperar', loadChildren: './recuperar-password/recuperar-password.module#RecuperarPasswordPageModule' },
      { path: 'registrar', loadChildren: './registrar-usuario/registrar-usuario.module#RegistrarUsuarioPageModule' },
      {
        path: 'mensaje-confirmacion-login',
        loadChildren: './mensaje-confirmacion-login/mensaje-confirmacion-login.module#MensajeConfirmacionLoginPageModule'
      }

    ]
  },
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class LoginRoutingModule { }
