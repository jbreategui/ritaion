import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { MensajeConfirmacionLoginPage } from './mensaje-confirmacion-login.page';
const routes: Routes = [
  {
    path: '',
    component: MensajeConfirmacionLoginPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)

  ],
  declarations: [MensajeConfirmacionLoginPage]
})
export class MensajeConfirmacionLoginPageModule {}
