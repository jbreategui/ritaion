import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { PagExtPage } from './pag-ext.page';
import { PagesExtRoutingModule } from './pag-ext-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';



@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReactiveFormsModule,
    PagesExtRoutingModule
  ],
  declarations: [PagExtPage]
})
export class PagExtPageModule { }
