import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PagesPage } from './pages.page';
import { LoginGuard } from '../services/guards/login.guard';
import { VerificaTokenGuard } from '../services/guards/verifica-token.guard';
const routes: Routes = [
  {
    path: '', component: PagesPage, children: [
      { path: '', redirectTo: 'pag-int' },
      {
        path: 'pag-ext',
        loadChildren: './pag-ext/pag-ext.module#PagExtPageModule'
      },
      {
        canActivate: [LoginGuard, VerificaTokenGuard],
        path: 'pag-int', loadChildren: './pag-int/pag-int.module#PagIntPageModule'
      }

    ]
  },

];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class PagesRoutingModule { }
