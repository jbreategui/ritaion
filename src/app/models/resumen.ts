export class Resumen {
    constructor(
        public fruta?: Fruta,
        public topping?: Topping

    ) { }
}

export class Fruta {
    constructor(
        public fruta_id: number = null,
        public fruta_nombre: string = '',
        public fruta_url: string = '',
        public fruta_precio: number = null
    ) { }
}

export class Topping {
    constructor(
        public topping_id: string = '',
        public topping_nombre: string = '',
        public topping_url: string = '',
        public topping_precio: string = ''
    ) { }
}