export interface VariablesEntorno {
  DURATIONSNACK: number, //en milisegundos
  API_KEY: string, //key del goople maps
  NUMERO_MAX_PETICIONES: number,
  INTERVALO_PETICIONES: number,  //segundos
  MOD_NUMERO_MAX_PETICIONES: number,
  MOD_INTERVALO_PETICIONES: number,  //segundos
  TIEMPO_PANTALLA_CARGA: number, //segundos
  TOKEN_DIAS: number, // cada cuántos dias  debe renovar el token 

}