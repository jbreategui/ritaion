export interface ResRecoger {
    estado: boolean;
    mensaje: string;
    status: number;
    payload: Recoger[];
}

export interface Recoger {
    pedido_id: number;
    fruta_id: number;
    fruta_nombre: string;
    fruta_precio: string;
    topping_id?: number;
    topping_nombre?: string;
    topping_precio?: string;
    jugo_precio: string;

}

export interface Bodypedido {
    idPedido: number;
    jugo_precio: number;
    bodyTopping: BodyTopping[];
    bodyFruta: BodyFruta[];
}
export interface BodyFruta {
    fruta_id: number;
    fruta_nombre: string;
    fruta_precio: string;
}

export interface BodyTopping {
    topping_id: number;
    topping_nombre: string;
    topping_precio: string;
}
