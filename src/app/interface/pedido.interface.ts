
export interface ResMapaRita {
    estado: boolean;
    mensaje: string;
    status: number;
    payload: MapaRita[];
}

export interface MapaRita {
    contrato_rita_ubicacion?: string;
    contrato_rita_referencia?: string;
    contrato_rita_coordenada_x?: string;
    contrato_rita_coordenada_y?: string;
    rita_codigo?: string;
    rita_id?: number;
}


export interface ResFrutas {
    estado: boolean;
    mensaje: string;
    status: number;
    payload: Fruta[];
}

export interface Fruta {
    fruta_id?: number;
    fruta_nombre?: string;
    fruta_url?: string;
    fruta_precio?: number;

}



export interface ResTopping {
    estado: boolean;
    mensaje: string;
    status: number;
    payload: Topping[];
}

export interface Topping {
    topping_id?: number;
    topping_nombre?: string;
    topping_url?: string;
    topping_precio?: number;
}


export interface Pago {
    documentoId: number;
    personaDocumento: string;
    terminoCondiciones: boolean;
    medioPagoId: string;
}

export interface Pedido {
    formaPago: number;
    usuario: number;
    montoTotal: number;
    mapaRita: MapaRita;
    fruta: Fruta[];
    pago?: Pago;
    topping?: Topping;

}
