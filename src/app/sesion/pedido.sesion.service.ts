import { Injectable } from '@angular/core';
import { MapaRita, Fruta, Topping, Pago } from '../interface/pedido.interface';

@Injectable({
  providedIn: 'root'
})
export class PedidoSesionService {

  constructor() { }
  public datosMapaRita: MapaRita = null;
  public datosFruta: Fruta[] = [];
  public datosTopping: Topping = null;
  public pago: Pago = null;
  public precioBase = 3.9; // precio base
  public formaPago = 0;
  public montoTotal = 0.0;
  public pedidoId = 0;
  public qrPedido = null;
  public ritaId = null;
  public ritaCodigoSocket = '1mark1';
  public modbuss = null;
  public splashLoad = false;
}
