import { Component, OnInit, Input } from '@angular/core';
@Component({
  selector: 'app-qr-component',
  templateUrl: './qr-component.page.html',
  styleUrls: ['./qr-component.page.scss'],
})
export class QrComponentPage implements OnInit {
  title = 'app';
  elementType = 'url';
  llave = 'proyectoAngular';
  @Input() mensaje = '';
  @Input() value = '';
  @Input() cargando = false;
  constructor() { }

  ngOnInit() {
 
  }

}
