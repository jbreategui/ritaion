import { Component, OnInit, EventEmitter, Output, OnDestroy } from '@angular/core';
import { FormGroup, Validators, FormControl } from '@angular/forms';
import { PedidoService } from 'src/app/services/pedido/pedido.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { DURATIONSNACK } from 'src/app/config/variables';
import * as $ from 'jquery';
@Component({
  selector: 'app-registro-datos-bancarios',
  templateUrl: './registro-datos-bancarios.component.html',
  styleUrls: ['./registro-datos-bancarios.component.scss'],
})
export class RegistroDatosBancariosComponent implements OnInit {


  @Output() metodoRB: EventEmitter<any>;

  formulario: FormGroup;
  opt = {

    initialSlide: 0,
    direction: 'horizontal',
    speed: 300,
    effect: 'slide',
    spaceBetween: 8,
    slidesPerView: 2.1,
    freeMode: true,
    loop: false

  }

  public documento_id: string;
  public documento: string;
  public lista_tipo_documento: any[];
  public lista_tipo_pago: any[];
  constructor(private ws_pedido: PedidoService,
    private snackBar: MatSnackBar) {
    this.metodoRB = new EventEmitter();

  }

  ngOnInit() {

    this.formulario = new FormGroup({
      'documento_id': new FormControl('', Validators.required),
      'persona_documento': new FormControl('', Validators.required),
      'terminos_condiciones': new FormControl(false),
      'medios_pago_id': new FormControl('', Validators.required)
    });
    this.getListaPedidoPagar();
  }
  getInvoque() {
    if (this.validarFormulario()) {
      const body = this.formulario.value;
      this.metodoRB.emit(body);
      this.snackBar.dismiss();
    }
  }


  getListaPedidoPagar() {
    this.ws_pedido.getPagarPedido().subscribe((res: any) => {
      this.documento_id = res.payload.documento_id;
      this.documento = res.payload.persona_documento;
      this.lista_tipo_documento = res.payload.tipo_documento;
      this.lista_tipo_pago = res.payload.medios_pago;
      this.formulario.get('documento_id').setValue(this.documento_id);
      this.formulario.get('persona_documento').setValue(this.documento);
      console.log(this.lista_tipo_pago);
      // queda en crear otro combo apra  probar los valores despues de cargar
    });
  }



  selecconar_pago(id: number) {
    this.formulario.get('medios_pago_id').setValue(id);
    this.stilos(id);


  }
  public id = 0;
  stilos(s: number) {
    if (this.id == 0) {
      $('#' + s + '_pago').addClass('highlighs_pagos');
      this.id = s;
    } else {

      $('#' + this.id + '_pago').removeClass('highlighs_pagos');
      $('#' + s + '_pago').addClass('highlighs_pagos');
      this.id = s;
    }



  }

  validarFormulario() {

    if (this.formulario.controls['persona_documento'].invalid) {
      this.snackBar.open('Ingrese docuemento', 'cerrar', { duration: DURATIONSNACK });
      return false;
    } else if (this.formulario.controls['medios_pago_id'].invalid) {
      this.snackBar.open('Seleccione método de pago', 'cerrar', { duration: DURATIONSNACK });
      return false;
    } else if (!this.formulario.controls['terminos_condiciones'].value) {
      this.snackBar.open('Debe aceptar los términos y condiciones', 'cerrar', { duration: DURATIONSNACK });
      return false;
    } else {

      return true;
    }

  }
}
