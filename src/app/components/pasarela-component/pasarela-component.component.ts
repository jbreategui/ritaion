import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-pasarela-component',
  templateUrl: './pasarela-component.component.html',
  styleUrls: ['./pasarela-component.component.scss'],
})
export class PasarelaComponentComponent implements OnInit {
  @Output() output: EventEmitter<any>;
  constructor(
    public snackBar: MatSnackBar
  ) { this.output = new EventEmitter(); }

  ngOnInit() {
  }
  enviar() {

  }
  regresarDatos() {
    this.output.emit(true);
  }
}
