import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { RegistroDatosBancariosComponent } from './registro-datos-bancarios/registro-datos-bancarios.component';
import { ResumenComponent } from './resumen/resumen.component';
import { IonicModule } from '@ionic/angular';
import { QrComponentPage } from './qr-component/qr-component.page';
import { NgxQRCodeModule } from 'ngx-qrcode2';
import { PasarelaComponentComponent } from './pasarela-component/pasarela-component.component';




@NgModule({
  declarations: [
    RegistroDatosBancariosComponent,
    ResumenComponent,
    QrComponentPage,
    PasarelaComponentComponent

  ]
  ,
  imports: [
    CommonModule,
    ReactiveFormsModule,
    MatSnackBarModule,
    FormsModule,
    IonicModule,
    NgxQRCodeModule

  ],
  exports: [
    RegistroDatosBancariosComponent,
    ResumenComponent,
    QrComponentPage,
    PasarelaComponentComponent
  ]
})
export class ComponentsModule { }
