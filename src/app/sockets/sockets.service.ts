import { Injectable } from '@angular/core';
import { Socket } from 'ngx-socket-io';
import { Router } from '@angular/router';
import { MetodosNativosService } from '../services/nativo/metodos-nativos.service';
import * as eventos from '../config/rutas-sockets';
import { take } from 'rxjs/operators';
import { HttpHeaders } from '@angular/common/http';
import { MetodosService } from '../services/metodos/metodos.service';
import { LoadingScreenService } from '../services/loading-screen/loading-screen.service';
@Injectable({
  providedIn: 'root'
})
export class SocketsService {
  public socketEstado = true;
  constructor(
    private socket: Socket,
    private route: Router,
    private nat: MetodosNativosService,
    private metodos: MetodosService,
    private loadingScreenService: LoadingScreenService

  ) { }
  verificarConexion() {
    this.socket.on('connect', () => {
      console.log('conectado');
      this.socketEstado = true;
      this.configurarUsuario();

      console.log('session socket- >', this.socketEstado);
    });


    this.socket.on('disconnect', () => {
      console.log('desconectado del servidor');
      this.nat.obtenerLocalStorage('ID_TABLA').then((item: any) => {
        this.eliminarUsuarioSocket(item);
      });
      console.log('session socket- >', this.socketEstado);
      this.socketEstado = false;
    });
  }

  configurarUsuario() {
    return new Promise((resolve, reject) => {

      this.nat.obtenerLocalStorage('usuario').then((item: any) => {
        let name = null;
        let id = null;

        if (item) {
          console.log(item);
          name = item.name;
          id = item.usuario_id;
          this.escucharInicioSesion()
            .pipe(take(1)).subscribe((res: any) => {
              console.log(res);
              this.nat.guardarLocalStorage('ID_TABLA', res.payload);

              resolve();
            }, err => {
              console.log(err);
              this.nat.guardarLocalStorage('ID_TABLA', null);
              reject(err);

            }, () => { 
              console.log('terminado');
            });

          this.emitir(eventos.CONFIGURAR_USUARIO, { nombre: name, idUsuario: id }, (res: any) => { });
        }

      }, (res1) => {
        console.log(res1);
      });

    });
  }
  emitir(evento: string, payload?: any, callback?: (res: any) => void) {

    this.socket.emit(evento, payload, callback);
    console.log('emitiendo -> ', evento);
  }

  escuchar(evento: string) {
    return this.socket.fromEvent(evento);
  }

  escucharInicioSesion() {
    return this.escuchar(eventos.ESUCHAR_USUARIO_INCIADO);
  }

  esucharError() {
    return this.escuchar(eventos.ESUCHAR_ERROR);
  }
  eliminarUsuarioSocket(id: string) {
    const headers = new HttpHeaders({
      Authorization: ''
    });
    return this.metodos.getQuery(eventos.ELIMINAR_USUARIO + '/' + id, headers)
      .subscribe((res => {
        this.nat.limpiarLocalStorage('token');
        this.nat.limpiarLocalStorage('usuario');
        this.route.navigate(['/login']);
      }));


  }

}
