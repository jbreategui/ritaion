import { Component, OnInit } from '@angular/core';

import { Validators, FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material/snack-bar';
import { LoadingController } from '@ionic/angular';
import { Usuario } from '../models/usuario';
import { ResultadoWS } from '../models/resultadoWS';
import { UsuarioService } from '../services/usuario/usuario.service';
import { MetodosNativosService } from '../services/nativo/metodos-nativos.service';
import { SocketsService } from '../sockets/sockets.service';
import { DURATIONSNACK } from '../config/variables';
import { PedidoSesionService } from '../sesion/pedido.sesion.service';
import { LoadingScreenService } from '../services/loading-screen/loading-screen.service';
@Component({
  selector: 'app-iniciar-sesion',
  templateUrl: './iniciar-sesion.page.html',
  styleUrls: ['./iniciar-sesion.page.scss'],
})
export class IniciarSesionPage implements OnInit {
  formulario: FormGroup;
  objUsuario: Usuario;
  email: string;
  recuerdame = false;
  ResultadoWS: ResultadoWS;
  loading: boolean;



  constructor(
    private service: UsuarioService,
    private router: Router,
    private snackBar: MatSnackBar,
    public loadingController: LoadingController,
    public nat: MetodosNativosService,
    public sockets: SocketsService,
    public pedido: PedidoSesionService,
    private loadingScreenService: LoadingScreenService
  ) {

  }
  ngOnInit() {
    this.formulario = new FormGroup({
      name: new FormControl('', Validators.required),
      password: new FormControl('', Validators.required)
    });

    this.pedido.splashLoad = true;
  }
  ionViewWillEnter() {

    this.caragrDatos();

  }

  caragrDatos() {
    this.nat.obtenerLocalStorage('name').then((res) => {
      this.email = res;
      this.formulario.get('name').setValue(this.email);
    });
  }

  login() {
    this.loadingScreenService.startLoading();
    if (this.sockets.socketEstado) {
      if (this.validarFormulario()) { // valida los inputs del formulario
        this.loading = true;

        this.getAutenticar().then(() => { // promesa ,epsera hasta obtener el token para cnsultar ws de los datos del usuario
          this.service.getUsuario().subscribe((res: any) => { // si devuelve una respuesta true , ejecuta el codigo interno        
            this.loading = false;

            if (res.estado) {
              this.nat.guardarLocalStorage('usuario', res.payload).then(() => {
                this.sockets.configurarUsuario().then(() => {

                  this.router.navigate(['/pages/pag-int/home']).then(() => {
                    this.loadingScreenService.stopLoading();
                  });
                }, () => {
                  this.snackBar.open('No se puede conectar al servidor', 'Cerrar', { duration: DURATIONSNACK });

                });
              });
            } else {
              this.snackBar.open(res.mensaje, 'Cerrar', { duration: DURATIONSNACK });
            }

          });
        }).catch(er => {

          this.snackBar.open(JSON.stringify(er), 'Cerrar', { duration: DURATIONSNACK });
          this.loading = false;

        });

      }
    } else {
      this.snackBar.open('Error en la aplicación', 'Cerrar', { duration: 3000 });
      this.loading = false;

    }

  }
  /* metodo para ejecutar el login */
  getAutenticar(): Promise<boolean> {
    let error = new ResultadoWS();
    return new Promise((resolve, reject) => {
      this.objUsuario = this.formulario.value;
      this.service.login(this.objUsuario).subscribe((res: ResultadoWS) => {
        if (res.estado) {
          let strToken = `${res.payload.access_token}`;
          let expires_in = res.payload.expires_in;
          this.service.guardarStorage(strToken);
          this.service.token = strToken;
          this.service.tokenExpire = res.payload.expires_in;
          resolve(true);
        }
        else {
          reject(res.mensaje);
        }
      }, error => {
        reject(error);
      });
    });
  }


  validarFormulario() {

    if (this.formulario.controls['name'].invalid) {
      this.snackBar.open('Ingrese correo', 'cerrar', { duration: DURATIONSNACK });
      return false;
    }
    else if (this.formulario.controls['password'].invalid) {
      this.snackBar.open('Ingrese contraseña', 'cerrar', { duration: DURATIONSNACK });
      return false;
    }

    else {

      return true;
    }

  }

  registrar() {
    this.router.navigate(['/pages/pag-ext/registrar']);
  }


  ubicanos() {
    this.router.navigate(['/pages/pag-ext/menu-publico/ubicanos']);
  }

  contactanos() {
    this.router.navigate(['/pages/pag-ext/menu-publico/contactanos-public']);
  }

}
