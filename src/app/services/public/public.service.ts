import { Injectable } from '@angular/core';
import { MetodosService } from '../metodos/metodos.service';
import { WS_LISTAR_TIPO_CONSULTAS, WS_REGISTRAR_CONSULTAS } from '../../config/rutas';

@Injectable({
  providedIn: 'root'
})
export class PublicService {

  constructor(private metodos: MetodosService) { }

  listarTipoConsulta() {

    return this.metodos.postQuery(WS_LISTAR_TIPO_CONSULTAS, null, null);
  }
  registrarConsulta(body: any) {
    return this.metodos.postQuery(WS_REGISTRAR_CONSULTAS, body, null);


  }
}
