import { Injectable } from '@angular/core';
import { MetodosService } from '../metodos/metodos.service';
import { HttpHeaders } from '@angular/common/http';
import { UsuarioService } from '../usuario/usuario.service';
import { ResMapaRita } from '../../interface/pedido.interface';

@Injectable({
  providedIn: 'root'
})
export class MapaService {

  constructor(private metodo: MetodosService, private usuario: UsuarioService) { }

  listaRitas() {
    const Authorization = `Bearer ${this.usuario.token}`;
    const headers = new HttpHeaders({
      Authorization
    });

    return this.metodo.postQuery<ResMapaRita>('lista_ritas', null, headers);

  }
  listaRitasPublic() {

    return this.metodo.postQuery<ResMapaRita>('lista_ritas_public', null, null);

  }

}