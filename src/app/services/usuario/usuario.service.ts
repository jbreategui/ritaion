import { Injectable } from '@angular/core';

import { map } from 'rxjs/operators';
import { HttpHeaders } from '@angular/common/http';
import { MetodosService } from '../metodos/metodos.service';
import { Usuario } from '../../models/usuario';
import { Router } from '@angular/router';
import { WS_RESET_PASSWORD, WS_USUARIO, WS_GET_USUARIO, WS_USUARIO_UPDATE, WS_CREATE_USUARIO } from '../../config/rutas';
import { MetodosNativosService } from '../nativo/metodos-nativos.service';
import { SocketsService } from '../../sockets/sockets.service';

@Injectable({
  providedIn: 'root'
})
export class UsuarioService {

  token: string;
  public usuario: Usuario = new Usuario();
  tokenExpire: string;

  constructor(
    private metodos: MetodosService,
    private router: Router,
    private nat: MetodosNativosService,
    private socket: SocketsService) {
    this.cargarStorage();
  }


  prueba() {
    const headers = new HttpHeaders({
      Authorization: ''
    });
    return this.metodos.getQuery(`fruta`, headers)
      .pipe(map(lista => lista));

  }



  login(obj: Usuario, recordar: boolean = true) {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json'
    });
    if (recordar) {
      this.nat.guardarLocalStorage('name', obj.name)
    } else {
      this.nat.limpiarLocalStorage('name');
    }

    let body = JSON.stringify(obj);
    return this.metodos.postQuery('auth/login', body, headers);

  }

  guardarStorage(token: string) {


    this.nat.guardarLocalStorage('token', token);
    this.token = token;


  }

  load() {
    return new Promise<string>((res, rej) => {
      this.nat.obtenerLocalStorage('token').then((item) => {
        res(item);
      });
    });

  }



  async  cargarStorage() {
    const token = await this.load();
    if (token !== null) {
      this.token = token;
      return token;
    } else {
      this.token = '';
      return '';
    }

  }


  async estaLogueado() {
    const token = await this.cargarStorage();
    return (token.length > 5) ? true : false;
  }

  logout() {

    const headers = new HttpHeaders({
      Authorization: `Bearer ${this.token}`
    });

    this.metodos.postQuery('auth/logout', '', headers)
      .subscribe((res) => {
        this.clearLocalStorahe();
        this.router.navigate(['/login']);
        console.log('deslogeado');
      });

  }
  clearLocalStorahe() {
    this.token = '';
    this.tokenExpire = '';
    this.nat.limpiarLocalStorage('token');
    this.nat.limpiarLocalStorage('usuario');
  }

  renuevaToken() {

    const headers = new HttpHeaders({
      Authorization: `Bearer ${this.token}`
    });

    return this.metodos.postQuery('auth/refresh', '', headers);


  }

  getUsuario() {

    const headers = new HttpHeaders({
      Authorization: `Bearer ${this.token}`
    });

    return this.metodos.postQuery('auth/me', '', headers);

  }
  getRecuperarPassword(body: string) {
    const headers = new HttpHeaders({
      Authorization: ``
    });
    console.log(body);
    return this.metodos.postQueryJon(WS_RESET_PASSWORD, body, headers);
  }
  getDatosUsuario(body: string) {
    const headers = new HttpHeaders({
      Authorization: `Bearer ${this.token}`
    });

    return this.metodos.postQueryJon(WS_GET_USUARIO, body, headers);
  }

  getUpdateUsuario(body: string) {
    const headers = new HttpHeaders({
      Authorization: `Bearer ${this.token}`
    });

    return this.metodos.postQueryJon(WS_USUARIO_UPDATE, body, headers);
  }
  registrarUsuario(body: string) {
    const headers = new HttpHeaders({
      Authorization: ``
    });

    return this.metodos.postQueryJon(WS_CREATE_USUARIO, body, headers);
  }
  // falta mostrar datos del usuario en el html


}
