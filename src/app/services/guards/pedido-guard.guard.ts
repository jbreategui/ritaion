import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { PedidoService } from '../pedido/pedido.service';


@Injectable({
  providedIn: 'root'
})
export class PedidoGuardGuard implements CanActivate {
  canActivate() {

    const bol = this.pedido.sessionPedido;
    if (!bol) {
      this.router.navigate(['/pages/pag-int/home']);
      return false;
    }
    return true;
  }
  constructor(private router: Router, private pedido: PedidoService) {

  }

}
