import { Injectable } from '@angular/core';
import {  CanActivate, Router } from '@angular/router';
import { PagarService } from '../pago/pagar.service';

@Injectable({
  providedIn: 'root'
})
export class PagarGuard implements CanActivate {
  canActivate() {

    const bol = this.pedido.sessionPagar;
    if (!bol) {
      this.router.navigate(['/pages/pag-int/home']);
      return false;
    }
    return true;
  }
  constructor(private router: Router, private pedido: PagarService) {

  }
}
