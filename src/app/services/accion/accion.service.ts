import { Injectable } from '@angular/core';
import { HttpHeaders } from '@angular/common/http';
import { WS_ACCION } from '../../config/rutas';
import { UsuarioService } from '../usuario/usuario.service';
import { MetodosService } from '../metodos/metodos.service';

@Injectable({
  providedIn: 'root'
})
export class AccionService {

  constructor(private usuario: UsuarioService, private metodo: MetodosService) { }
  getListaAcciones() {
    const headers = new HttpHeaders({
      'Authorization': `Bearer ${this.usuario.token}`
    });
    return this.metodo.postQuery(WS_ACCION, '', headers);
  }
}
