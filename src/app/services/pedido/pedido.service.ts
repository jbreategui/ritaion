import { Injectable } from '@angular/core';
import { MetodosService } from '../metodos/metodos.service';
import { HttpHeaders } from '@angular/common/http';
import { UsuarioService } from '../usuario/usuario.service';
import {
  WS_GET_FRUTAS,
  WS_SELEC_FRUTA,
  WS_GET_TOPPINGS,
  WS_SELEC_TOPPING,
  WS_PEDIDO_RESUMEN,
  WS_SELEC_RITA,
  WS_LIMPIAR_PEDIDO,
  WS_PAGAR,
  WS_CONFIRMAR_PAGO,
  WS_REGISTRO_PAGO
} from '../../config/rutas';
import { ResFrutas, ResTopping, Pedido, Pago } from '../../interface/pedido.interface';
import { WS_REGISTRO_PEDIDO_APP_PAGADO, WS_REGISTRAR_PAGO_APP } from '../../config/rutas';
import { PedidoSesionService } from '../../sesion/pedido.sesion.service';
import { MetodosNativosService } from '../nativo/metodos-nativos.service';


@Injectable({
  providedIn: 'root'
})
export class PedidoService {
  sessionPedido = false;
  constructor(
    private user: UsuarioService,
    private metodos: MetodosService,
    private session: PedidoSesionService,
    private nat: MetodosNativosService

  ) { }

  getListaFrutas() {

    const headers = new HttpHeaders({
      Authorization: `Bearer ${this.user.token}`
    });

    return this.metodos.postQuery<ResFrutas>(WS_GET_FRUTAS, '', headers);

  }
  registrarFruta(body: string) {

    const headers = new HttpHeaders({
      Authorization: `Bearer ${this.user.token}`
    });

    return this.metodos.postQueryJon(WS_SELEC_FRUTA, body, headers);

  }
  getListaToppings() {
    const headers = new HttpHeaders({
      Authorization: `Bearer ${this.user.token}`
    });

    return this.metodos.postQuery<ResTopping>(WS_GET_TOPPINGS, '', headers);

  }
  registrartopping(body: string) {

    const headers = new HttpHeaders({
      Authorization: `Bearer ${this.user.token}`
    });

    return this.metodos.postQueryJon(WS_SELEC_TOPPING, body, headers);

  }

  getResumenPedido() {
    const headers = new HttpHeaders({
      Authorization: `Bearer ${this.user.token}`
    });

    return this.metodos.postQuery(WS_PEDIDO_RESUMEN, '', headers);
  }
  registrarRita(body: string) {

    const headers = new HttpHeaders({
      Authorization: `Bearer ${this.user.token}`
    });

    return this.metodos.postQueryJon(WS_SELEC_RITA, body, headers);

  }
  getLimpiarPedido() {
    const headers = new HttpHeaders({
      Authorization: `Bearer ${this.user.token}`
    });

    return this.metodos.postQuery(WS_LIMPIAR_PEDIDO, '', headers);

  }


  getPagarPedido() {
    const headers = new HttpHeaders({
      Authorization: `Bearer ${this.user.token}`
    });

    return this.metodos.postQuery(WS_PAGAR, '', headers);

  }
  getConfirmarPago(body: string) {
    const headers = new HttpHeaders({
      Authorization: `Bearer ${this.user.token}`
    });

    return this.metodos.postQueryJon(WS_CONFIRMAR_PAGO, body, headers);
  }

  registrarPago() {
    const headers = new HttpHeaders({
      Authorization: `Bearer ${this.user.token}`
    });

    return this.metodos.postQuery(WS_REGISTRO_PAGO, '', headers);
  }

 


  registrarPedidoAppPagado(pedido: Pedido) {
    const headers = new HttpHeaders({
      Authorization: `Bearer ${this.user.token}`
    });
    console.log(pedido);
    return this.metodos.postQuery(WS_REGISTRO_PEDIDO_APP_PAGADO, { pedido }, headers);
  }

  registrarPagoApp(pedido: any) {
    const headers = new HttpHeaders({
      Authorization: `Bearer ${this.user.token}`
    });
    return this.metodos.postQuery(WS_REGISTRAR_PAGO_APP, pedido, headers);
  }


  iniciarPedido(evento: any) {

    return new Promise((ok, error) => {
      this.nat.obtenerLocalStorage('usuario').then((usuario: any) => {
        this.session.pago = null;
        const pago: Pago = {
          documentoId: evento.documento_id,
          personaDocumento: evento.persona_documento,
          terminoCondiciones: evento.terminos_condiciones,
          medioPagoId: evento.medios_pago_id
        };

        this.session.pago = pago;
        const pedido: Pedido = {
          formaPago: this.session.formaPago,
          montoTotal: this.session.montoTotal,
          usuario: usuario.usuario_id,
          mapaRita: this.session.datosMapaRita,
          fruta: this.session.datosFruta,
          topping: this.session.datosTopping,
          pago: this.session.pago
        };

        this.registrarPedidoAppPagado(pedido).subscribe(res => {
          ok(res);
        }, () => {
          error();
        });


      });
    });

  }

}
