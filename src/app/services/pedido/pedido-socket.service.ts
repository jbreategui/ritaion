import { Injectable } from '@angular/core';
import { SocketsService } from '../../sockets/sockets.service';
import { NOTIFICAR_NUEVO_USUARIO, ESCUCHAR_RITA_DISPONIBLE, ENVIAR_CONFIRMACION_CLIENTE } from '../../config/rutas-sockets';

@Injectable({
  providedIn: 'root'
})
export class PedidoSocketService {

  constructor(private socket: SocketsService) { }


  emitirNuevoTicket(idRita: string, qr: string, clienteId: number, funcion?: (res: any) => void) {
    this.socket.emitir(NOTIFICAR_NUEVO_USUARIO, { idRita, qr, clienteId }, funcion);
  }


  escucharRitaEstado() {
    return this.socket.escuchar(ESCUCHAR_RITA_DISPONIBLE);
  }


}
