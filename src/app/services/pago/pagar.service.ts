import { Injectable } from '@angular/core';
import { MetodosService } from '../metodos/metodos.service';
import { HttpHeaders } from '@angular/common/http';
import { WS_RECOGER_VALIDAR_CODIGO } from '../../config/rutas';
import { UsuarioService } from '../usuario/usuario.service';

@Injectable({
  providedIn: 'root'
})
export class PagarService {
  public sessionPagar = null;
  public pedidioIDRecoger = null;
  public pedidioIDqr = null;
  constructor(private metodos: MetodosService, private user: UsuarioService) { }

  getValidarCodigo(body: any) {
    const headers = new HttpHeaders({
      Authorization: `Bearer ${this.user.token}`
    });
    return this.metodos.postQuery(WS_RECOGER_VALIDAR_CODIGO, body, headers);
  }

}
