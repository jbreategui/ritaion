export const CONFIGURAR_USUARIO = 'configurar-usuario';
export const NOTIFICAR_NUEVO_USUARIO = 'notificar-nuevo-ticket';
export const ESCUCHAR_RITA_DISPONIBLE = 'escuchar-rita-disponible';
export const ENVIAR_CONFIRMACION_CLIENTE = 'enviar-confirmacion-cliente';
export const ESUCHAR_USUARIO_INCIADO = 'usuario-iniciado';
export const ESUCHAR_ERROR = 'notificar-error';
export const ELIMINAR_USUARIO = 'eliminar_usuario_socket_por_WS';
