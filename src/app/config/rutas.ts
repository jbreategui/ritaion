export const WS_UBIGEO = 'perfil/listar_ciudades';
export const WS_RESET_PASSWORD = 'reset_password';
export const WS_ACCION = 'perfil/listar_que_hacer';
export const WS_USUARIO = 'perfil/usuario';
export const WS_GET_FRUTAS = 'listar_frutas';
export const WS_SELEC_FRUTA = 'pedido/fruta/seleccionar';
export const WS_GET_TOPPINGS = 'pedido/listar_toppings';
export const WS_SELEC_TOPPING = 'pedido/topping/seleccionar';
export const WS_PEDIDO_RESUMEN = 'pedido/resumen';
export const WS_SELEC_RITA = 'rita/seleccionar';
export const WS_LIMPIAR_PEDIDO = 'pedido/limpiar';
export const WS_PAGAR = 'pagar/datos_usuario'
export const WS_CONFIRMAR_PAGO = 'pagar/confirmar'
export const WS_REGISTRO_PAGO = 'pagar/registrar_compra'


export const WS_REGISTRO_PEDIDO_APP_PAGADO = 'pedido/crear_pedido_pagado';
export const WS_REGISTRAR_PAGO_APP = 'pedido/confirmar_pago_app';


// recoger

export const WS_LISTAR_PEDIDOS = 'recoger/listar_pedidos_por_usuario';
export const WS_SELECCIONAR_PEDIDO = 'recoger/seleccionar_pedido';
export const WS_RECOGER_RESUMEN_PEDIDO = 'recoger/resumen_pedido';
export const WS_RECOGER_PEDIDO = 'recoger/pedido';
export const WS_RECOGER_LISTAR_JUGO = 'recoger/listar_jugos_por_pedido';
export const WS_RECOGER_VALIDAR_CODIGO = 'recoger/validar_codigo_pedido';




export const WS_GET_USUARIO = 'perfil/usuario';
export const WS_USUARIO_UPDATE = 'perfil/update/usuario';
export const WS_CREATE_USUARIO = 'crear_usuario';
//

export const WS_LISTAR_TIPO_CONSULTAS = 'listar_tipo_consulta';
export const WS_REGISTRAR_CONSULTAS = 'registrar_consulta';

