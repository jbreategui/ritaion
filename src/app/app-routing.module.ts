import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { SplashGuard } from './services/guards/splash.guard';

const routes: Routes = [
  { path: '', redirectTo: 'pages', pathMatch: 'full' },
  { path: 'pages', canLoad: [SplashGuard], loadChildren: './pages/pages.module#PagesPageModule' },
  { path: 'login', loadChildren: './iniciar-sesion/iniciar-sesion.module#IniciarSesionPageModule' }

];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
